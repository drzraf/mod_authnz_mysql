/*
 * This code exists for the sole purpose to serve as another implementation
 * of the "private" password hashing method implemened in PasswordHash.php
 * and thus to confirm that these password hashes are indeed calculated as
 * intended.
 *
 * Other uses of this code are discouraged.  There are much better password
 * hashing algorithms available to C programmers; one of those is bcrypt:
 *
 *	http://www.openwall.com/crypt/
 *
 * Written by Solar Designer <solar at openwall.com> in 2005 and placed in
 * the public domain.
 *
 * There's absolutely no warranty.
 *
 * Original code can be found at:
 * http://cvsweb.openwall.com/cgi/cvsweb.cgi/projects/phpass/c/crypt_private.c
 *
 * UPDATE: 2011, < raphael.droz+floss at gmail.com > :
 * - use of Apache Portable Runtime Library for MD5
 * - added SHA512 (from openssl)
 * - added '$H$' and '$S$' prefix for Drupal 7 passwords support
 *
 */

#include <string.h>
#include <stdlib.h>
#include <apr_md5.h>
#include <openssl/sha.h>

#ifdef TEST
#include <stdio.h>
#endif

#ifdef DEBUG
	#ifdef TEST
#define LOG(message, ...)	printf(message "\n", ##__VA_ARGS__)
	#else
#include <syslog.h>
#define LOG(message...)		syslog(LOG_DEBUG, message)
	#endif
#else
#define LOG(message...)
#endif


static char *itoa64 =
	"./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

static void encode64(char *dst, char *src, int count)
{
	int i, value;

	i = 0;
	do {
		value = (unsigned char)src[i++];
		*dst++ = itoa64[value & 0x3f];
		if (i < count)
			value |= (unsigned char)src[i] << 8;
		*dst++ = itoa64[(value >> 6) & 0x3f];
		if (i++ >= count)
			break;
		if (i < count)
			value |= (unsigned char)src[i] << 16;
		*dst++ = itoa64[(value >> 12) & 0x3f];
		if (i++ >= count)
			break;
		*dst++ = itoa64[(value >> 18) & 0x3f];
	} while (i < count);
}

char *crypt_private(char *password, char *setting)
{
	char *output = (char*) malloc(SHA512_DIGEST_LENGTH + 12);
	char *p, *salt;
	int count_log2, length, count;

	memset(output, 0, SHA512_DIGEST_LENGTH + 12);

	strcpy(output, "*0");
	if (!strncmp(setting, output, 2))
		output[1] = '1';

	if (strncmp(setting, "$P$", 3) && strncmp(setting, "$H$", 3) && strncmp(setting, "$S$", 3))
		return output;

	p = strchr(itoa64, setting[3]);
	if (!p)
		return output;
	count_log2 = p - itoa64;
	if (count_log2 < 7 || count_log2 > 31)
		return output;

	salt = setting + 4;
	if (strlen(salt) < 8)
		return output;

	length = strlen(password);
	count = 1 << count_log2;

	if(! strncmp(setting, "$S$", 3)) {
	  	SHA512_CTX ctx;
		unsigned char hash[SHA512_DIGEST_LENGTH];
		SHA512_Init(&ctx);
		SHA512_Update(&ctx, salt, 8);
		SHA512_Update(&ctx, password, length);
		SHA512_Final(hash, &ctx);

		do {
		  SHA512_Init(&ctx);
		  SHA512_Update(&ctx, hash, SHA512_DIGEST_LENGTH);
		  SHA512_Update(&ctx, password, length);
		  SHA512_Final(hash, &ctx);
		} while (--count);

		memcpy(output, setting, 12);
		encode64(&output[12], hash, SHA512_DIGEST_LENGTH);

		// thanks to DRUPAL_HASH_LENGTH...
		output[55] = 0;
	}
	else {
	  apr_md5_ctx_t ctx;
	  char hash[APR_MD5_DIGESTSIZE];

	  apr_md5_init(&ctx);
	  apr_md5_update(&ctx, salt, 8);
	  apr_md5_update(&ctx, password, length);
	  apr_md5_final(hash, &ctx);


	  do {
	    apr_md5_init(&ctx);
	    apr_md5_update(&ctx, hash, APR_MD5_DIGESTSIZE);
	    apr_md5_update(&ctx, password, length);
	    apr_md5_final(hash, &ctx);
	  } while (--count);

	  memcpy(output, setting, 12);
	  encode64(&output[12], hash, APR_MD5_DIGESTSIZE);
	}

	return output;
}

#ifdef TEST
int main(int argc, char **argv) {
  if (argc != 3) return 1;
  puts(crypt_private(argv[1], argv[2]));
  return 0;
}
#endif
