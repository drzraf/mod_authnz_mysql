mod_authnz_mysql
================

I tried to refurbish mod_auth_mysql based on http://packages.debian.org/squeeze/libapache2-mod-auth-mysql to make it more compatible with Apache 2.x

Short Changelog:

*	Dropped Apache 1.x support
*	Added "mysql" AuthBasicProvider
*	Removed a lot of redundant config options
*	Removed some deprecated config options
